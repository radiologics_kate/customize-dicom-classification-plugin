package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;
import org.nrg.attr.*;
import org.nrg.dcm.DicomAttributeIndex;
import org.nrg.dcm.FixedDicomAttributeIndex;

import java.util.Collections;
import java.util.Map;

public class NoteAttribute extends AbstractExtAttrDef<DicomAttributeIndex, String, String>
        implements XnatAttrDef {

    private static final DicomAttributeIndex index = new FixedDicomAttributeIndex(Tag.PatientComments);

    public NoteAttribute() {
        super("note", index);
    }

    @Override
    public Iterable<ExtAttrValue> apply(String a) throws ExtAttrException {
        if (null == a) {
            throw new NoUniqueValueException(this.getName());
        } else {
            return Collections.singletonList(new BasicExtAttrValue(this.getName(), a));
        }
    }

    @Override
    public String start() {
        return null;
    }

    @Override
    public String foldl(String a, Map<? extends DicomAttributeIndex, ? extends String> m) throws ExtAttrException {
        Object ov = m.get(index);
        if (null == ov) {
            return a;
        } else {
            // Replace some string in the note
            return ov.toString().replaceAll(".*NOTE:", "");
        }
    }
}
