package org.nrg.dcm.xnat;

import org.dcm4che2.data.Tag;
import org.nrg.attr.LabeledAttrDef;
import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;
import org.nrg.xdat.bean.XnatSmsessiondataBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class XnatSmSessionAttributes extends DICOMImageSessionAttributes {

    @Autowired
    public XnatSmSessionAttributes() {
        super(XnatSmsessiondataBean.class);
    }

    public AttrDefs get() { return s; }

    static final private MutableAttrDefs s = new MutableAttrDefs(ImageSessionAttributes.get());

    //static {
    //    // sample: populate a custom variable
    //    s.add(LabeledAttrDef.create(new XnatAttrDef.Text("fields/field", Tag.PatientAge), "name", "dcmage"));
    //}
}
