package org.nrg.dcm.xnat;

import org.nrg.dcm.AttrDefs;
import org.nrg.dcm.MutableAttrDefs;
import org.nrg.xdat.bean.XnatSmscandataBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class XnatSmScanAttributes extends DICOMImageScanAttributes {

    @Autowired
    public XnatSmScanAttributes() {
        super(XnatSmscandataBean.class);
    }

    @Override
    public AttrDefs get() { return s; }

    static final private MutableAttrDefs s = new MutableAttrDefs(ImageScanAttributes.get());

    //static {
    //    // sample: put PatientComments into scan "note" field
    //    s.add(new NoteAttribute());
    //}
}
