package com.radiologics.demo;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@XnatPlugin(value = "demoPlugin", name = "Demo Plugin")
@Configuration
@ComponentScan(value = "com.radiologics.demo")
public class DemoPlugin {
}